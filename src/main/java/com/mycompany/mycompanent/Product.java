/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycompanent;

import java.util.ArrayList;

/**
 *
 * @author PP
 */
public class Product {
    private int id ;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductlist(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"Espesso",40.0, "PD0001_real.jpg"));
        list.add(new Product(1,"Cha yen",35.0, "PD0002_real.jpg"));
        list.add(new Product(1,"Capocino",45.0, "PD0003_real.jpg"));
        list.add(new Product(1,"Latte" ,50.0, "PD0004_real.jpg"));
        list.add(new Product(1,"Green tea",55.0, "PD0005_real.jpg"));
        list.add(new Product(1,"Milk",60.0, "PD0006_real.jpg"));
        return list;
        
        
    }
}
